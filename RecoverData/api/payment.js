const express = require('express');
const router = express.Router();
const fs = require('fs');
const status_file_path = './status.txt';

var output = " ";
var transaction;

router.post('/payment',(req, res, next)=>{
    let receiver = req.body.payment.receiver;
    let transfer_receiver_mobile = req.body.payment.transfer_receiver_mobile;
    let amount = req.body.payment.amount_eq;
    let transaction_code = req.body.payment.transaction_code;
    let status = req.body.payment.status;
    let pin_code = req.body.pin_code;
    
    if (transaction != null)
        return res.status(400).json({
            message:'Handling POST requests to /payment',
            error: 'An operation is already pending'
        });
    if (amount == null || transaction_code == null || status == null || (receiver == null && transfer_receiver_mobile == null))
        return res.status(400).json({
            message:'Handling POST requests to /payment',
            error: 'missing params'
        });
    if (status !== 'PAID')
        return res.status(400).json({
            message:'Handling POST requests to /payment',
            error: 'payment not in paid status'
        });
    if (transfer_receiver_mobile == null && receiver.mobiles.length === 0)
        return res.status(400).json({
            message:'Handling POST requests to /payment',
            error: 'receiver has no mobile number'
        });
    if (transfer_receiver_mobile == null && receiver.mobiles.find((m) => m.mobile.startsWith('+237')) == null)
        return res.status(400).json({
            message:'Handling POST requests to /payment',
            error: 'receiver has no mobile number in Cameroon'
        });

    const payment = {
        receiver: transfer_receiver_mobile || receiver.mobiles.find((m) => m.mobile.startsWith('+237').mobile),
        amount: amount,
        transaction_code: transaction_code
    };

    if (!payment.receiver.startsWith('+237')) {
        data_error.error= true;
        data_error.amount = "error: uncorrect phone number format";
    } else {
        payment.receiver = payment.receiver.substring(4);
    }
    
    const data_error = {
        error: false,
        receiver:'',
        amount:'',
        transaction_code:'',
    };

    function verified_data(){

        if(payment.amount<=0){
            data_error.error= true;
            data_error.amount = "error: amount less than or equal 0";
        }
        if (payment.receiver.length !== 9){
            data_error.error = true;
            data_error.receiver = "error: receiver must have 9 characters"
        }else{
            let num = parseInt(payment.receiver.slice(0, 3),10);

            if((650 <= num && num <= 654) || (670 <= num && num <= 689)){
                payment.provider = "mtn";
                if(pin_code.length !== 5){
                    data_error.error = true;
                    data_error.transaction_code = "error: mtn pin code must have 5 characters"
                }
            }else{
                if((655<=num && num<=659) || (690<=num && num<=699)){
                    payment.provider = "orange";
                    if(pin_code.length !== 4){
                        data_error.error = true;
                        data_error.transaction_code = "error: orange pin code must have 4 characters"
                    }
                }
            }
            // tests purposes
            payment.provider = "test";
        }       
    }
    verified_data();

    if(data_error.error){
        res.status(200).json({
            message:'Handling POST requests to /payment',
            error: data_error
        });
    } else {
        exec_payment(payment.provider, payment.receiver, payment.amount, pin_code, function(log, err){
            transaction = null;
            if (err) {
                res.status(500).json({
                    message:'Handling POST requests to /payment',
                    error: "internal server error",
                    log: log
                });
            } else {
                if(check_status("status.txt") === "starting" || check_status(status_file_path) === "ussd_request"){
                    res.status(200).json({
                        message:'Handling POST requests to /payment',
                        result: output,
                        log: log
                    });
                }else {
                    res.status(200).json({
                        message:'Handling POST requests to /payment',
                        result: output,
                        log: log
                    });
                }
            }
            fs.unlinkSync(status_file_path);
        });
    }    
});

function exec_payment(provider, receiver, amount, code, callback){

    //we verified if a transaction is in progress before make another
    if(check_status(status_file_path) === "starting" || check_status(status_file_path) === "ussd_request"){
        output = "another transaction in progress. please wait";
        callback();
    }else{

        const { spawn } = require('child_process');
        transaction = spawn('java', ['-cp','USSD Test.jar','-Djava.library.path=/usr/lib/jni','UssdTest.MainClass', provider, receiver, amount, code]);
        let file = ""; 

        transaction.stdout.on('data',(data) => {
            if (data.toString().includes(">>> "))   
                output= data.toString();
            file += ("\n" + data.toString());
            console.log('stdout:', data.toString());			
        });

        transaction.stderr.on('data', (data) => {
            if (data.toString().includes(">>> ")) 
                output=data.toString();
            console.error('stderr:', data.toString());
            file += ("\n" + data.toString());
            //console.log('---');
            //callback(new Error(data.toString()));
        });

        transaction.on('close', (code) => {
            if (code === 0)
                callback(file);
            else callback(file, new Error("child process exited with code " + code));
        });
        
    }	
}

function close_stream(root) {
    root.stdout.close();
    root.stderr.close();
}

function check_status(path){
    if (!fs.existsSync(path))
        return null;
	var status = fs.readFileSync(path, 'utf-8');
	return status.toString();
}




///
router.post('/cancel',(req, res, next)=>{
    if (transaction == null) {
        return res.status(400).json({
            message:'Handling POST requests to /cancel',
            error: 'nothing to stop'
        });
    }

    transaction.stdin.setEncoding('utf-8');
    transaction.stdin.write('cancel\n');

    res.status(200).json({
        message:'Handling POST requests to /cancel',
        cancel: true
    });
  
});

module.exports = router;