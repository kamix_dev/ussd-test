const express = require('express');
const router = express.Router();


router.get('/',(req, res, next)=>{
    const payment_status = {
        status:''
    };

    payment_status.status=check_status('./status.txt');

        res.status(200).json({
            message:'Handling GET requests to /check_status',
            payment_status: payment_status
        });
  
});

function check_status(path){
    const fs = require('fs');
    if (!fs.existsSync(path))
        return null;
	var status = fs.readFileSync(path, 'utf-8')
	return status;
}

module.exports = router